let collection = [];

// Write the queue functions below.

// Output all the elements of the queue
function print() {
  return collection;
}

// Adds element to the rear of the queue
function enqueue(newItem) {

    collection.push(newItem);
    return collection;
}


// Removes element from the front of the queue
function dequeue(removeElement) {
  
  collection.shift(removeElement)
  return collection;

}

// Show element at the front
function front() {
  return collection[0]
}

// Show total number of elements
function size() {
   return collection.length
}

// Outputs a Boolean value describing whether queue is empty or not
function isEmpty() {

    if(collection.length === 0){
    	return true
    } else {
    	return false
    }
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
